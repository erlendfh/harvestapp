
import hours from './hours';
import auth from './auth';
import location from './location';

export default function* rootSaga() {
  yield [auth(), hours(), location()];
}
