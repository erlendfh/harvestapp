import { takeEvery, take } from 'redux-saga/effects';
import { fork, put, call, select } from 'redux-saga/effects';
import axios from 'axios';
import {
  UPDATE_LOCATION_SUCCESS,
} from '../redux/locationReducer';
import { getAccessToken } from '../redux/authReducer'
import { HARVEST_ROOT_URL, HARVEST_CONFIG } from '../utils/harvestService';
import { getDistanceBetweenTwoLocations } from '../utils/utils';

const workCoords = {
  latitude: '59.912270',
  longitude: '10.735820',
};

function* updateLocation(action) {
  const { latitude, longitude } = action.position.coords;
  const distanceBetweenTwoLocations = yield getDistanceBetweenTwoLocations(
    latitude,
    longitude,
    workCoords.latitude,
    workCoords.longitude,
    'K',
  );

  const accessToken = yield select(getAccessToken)

  if (distanceBetweenTwoLocations < 1) { // distance in K
    // TODO: check if task hasn't been started yet
    // TODO: if so, start the task
    const task = yield call(
      axios.get,
      `${HARVEST_ROOT_URL}${'/v2/time_entries/868727393'}`,
      {
        headers: {
          ...HARVEST_CONFIG.headers,
          Authorization: `Bearer ${accessToken}`,
        }
      }
    );


    console.log('CURRENT task ', task);

    if (task.data && !task.data.is_running) {
      console.log('START TIMER!');


      axios
        .patch(
          'https://api.harvestapp.com/v2/time_entries/868727393/restart',
          null,
          {
            headers: {
              ...HARVEST_CONFIG.headers,
              Authorization: `Bearer ${accessToken}`,
            }
          },
        )
        .then((r) => {
          console.log('jjjj ', r);
        })
        .catch(e => console.log(e.message));


    //   yield put({ type: FETCH_HOURS });
    }
  } else {
    // TODO: check if task is already running
    // TODO: if so, stop to the timer
    const task = yield call(
      axios.get,
      `${HARVEST_ROOT_URL}${'/v2/time_entries/868727393'}`,
      {
        headers: {
          ...HARVEST_CONFIG.headers,
          Authorization: `Bearer ${accessToken}`,
        }
      },
    );

    console.log('tttt ', task);

    if (task.data && task.data.is_running) {
      console.log('STOP TIMER ');
      const { data, status } = yield call(
        axios.get,
        `${HARVEST_ROOT_URL}${'/v2/time_entries/868727393/stop'}`,
        {
          headers: {
            ...HARVEST_CONFIG.headers,
            Authorization: `Bearer ${accessToken}`,
          }
        },
      );

      console.log('DDDDD ', data);
      console.log('STA ', status);
    }


    // const { data, status } = yield call(
    //   axios.get,
    //   `${HARVEST_ROOT_URL}${'/v2/time_entries/868727393/stop'}`,
    //   HARVEST_CONFIG,
    // );
  }

  return true;
}


function* watchUpdateLocation() {
  yield takeEvery(UPDATE_LOCATION_SUCCESS, updateLocation);
}

export default function* hours() {
  yield fork(watchUpdateLocation);
}
