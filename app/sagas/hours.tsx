import { takeEvery } from 'redux-saga/effects';
import { fork, put, call, select } from 'redux-saga/effects';
import axios from 'axios';
import {
  FETCH_HOURS,
  FETCH_HOURS_SUCCESS,
  FETCH_HOURS_FAILED,
} from '../redux/harvestReducer';
import { getAccessToken } from '../redux/authReducer'
import { HARVEST_ROOT_URL, HARVEST_CONFIG } from '../utils/harvestService';

// worker Saga: will be fired on USER_FETCH_REQUESTED actions
function* fetchHours() {
  const accessToken = yield select(getAccessToken)
  const { data, status } = yield call(
    axios.get,
    `${HARVEST_ROOT_URL}${'/v2/time_entries?user_id=2026476'}`,
    {
      headers: {
        ...HARVEST_CONFIG.headers,
        Authorization: `Bearer ${accessToken}`,
      }
    }
  );
  if (data && status === 200) {
    yield put({ type: FETCH_HOURS_SUCCESS, hours: data });
    return true;
  }
  yield put({ type: FETCH_HOURS_FAILED, hours: null, error: 'eeeee' });
}
/*
 Starts fetchUser on each dispatched `USER_FETCH_REQUESTED` action.
 Allows concurrent fetches of user.
 */
function* watchFetchHours() {
  yield takeEvery(FETCH_HOURS, fetchHours);
}
/*
 Alternatively you may use takeLatest.

 Does not allow concurrent fetches of user. If "USER_FETCH_REQUESTED" gets
 dispatched while a fetch is already pending, that pending fetch is cancelled
 and only the latest one will be run.
 */
export default function* hours() {
  yield fork(watchFetchHours);
}
