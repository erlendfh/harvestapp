import { takeEvery } from 'redux-saga/effects';
import { fork, put, call } from 'redux-saga/effects';
import axios from 'axios';
import {
  AUTH_LOGIN,
  LoginAction,
} from '../redux/authReducer';
import { HARVEST_ROOT_URL, HARVEST_CONFIG } from '../utils/harvestService';

// worker Saga: will be fired on USER_FETCH_REQUESTED actions
function* loginSaga(payload:LoginAction) {
  console.log('auth state', payload)
  const { data, status } = yield call(
    axios.get,
    `${HARVEST_ROOT_URL}${'/v2/users/me'}`,
    {
      headers: {
        ...HARVEST_CONFIG.headers,
        Authorization: `Bearer ${payload.accessToken}`,
      }
    }
  );
  console.log('data ', data);
  console.log('status ', status);
}
/*
 Starts fetchUser on each dispatched `USER_FETCH_REQUESTED` action.
 Allows concurrent fetches of user.
 */
function* watchLogin() {
  yield takeEvery(AUTH_LOGIN, loginSaga);
}
/*
 Alternatively you may use takeLatest.

 Does not allow concurrent fetches of user. If "USER_FETCH_REQUESTED" gets
 dispatched while a fetch is already pending, that pending fetch is cancelled
 and only the latest one will be run.
 */
export default function* mySaga() {
  yield fork(watchLogin);
}
