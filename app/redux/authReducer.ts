export const AUTH_LOGIN = 'AUTH_LOGIN';

export const getAccessToken = (state:{auth:AuthState}) => state.auth.accessToken

export interface LoginAction {
  type: typeof AUTH_LOGIN,
  accessToken: string,
}

export const login = (accessToken:string): LoginAction => ({
  type: AUTH_LOGIN,
  accessToken,
})

export type AuthActions =
  LoginAction

interface AuthState {
  accessToken: string
}

export default function (state:AuthState = {
  accessToken: '',
}, action:AuthActions) {
  switch (action.type) {
    case AUTH_LOGIN:
      return {
        ...state,
        accessToken: action.accessToken,
      };
    default:
      return state;
  }
}
