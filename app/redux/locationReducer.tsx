export const UPDATE_LOCATION = 'UPDATE_LOCATION';
export const UPDATE_LOCATION_SUCCESS = 'UPDATE_LOCATION_SUCCESS';
export const UPDATE_LOCATION_FAILED = 'UPDATE_LOCATION_FAILED';

export function updateLocationSuccess(position) {
  return {
    type: UPDATE_LOCATION_SUCCESS,
    position,
  };
}

export function updateLocationFailed(error) {
  return {
    type: UPDATE_LOCATION_FAILED,
    error,
  };
}

export default function (state = [], action) {
  switch (action.type) {
    case UPDATE_LOCATION_SUCCESS:
      return {
        ...state,
        position: action.position,
        error: null,
      };
    case UPDATE_LOCATION_FAILED:
      return {
        ...state,
        coords: null,
        error: action.error,
      };
    default:
      return state;
  }
}
