export const FETCH_HOURS = 'FETCH_HOURS';
export const FETCH_HOURS_SUCCESS = 'FETCH_HOURS_SUCCESS';
export const FETCH_HOURS_FAILED = 'FETCH_HOURS_FAILED';

export function fetchHours() {
  return {
    type: FETCH_HOURS,
  };
}

export default function (state = [], action) {
  switch (action.type) {
    case FETCH_HOURS_SUCCESS:
      return {
        ...state,
        hours: action.hours,
        error: null,
      };
    case FETCH_HOURS_FAILED:
      return {
        ...state,
        hours: null,
        error: action.error,
      };
    default:
      return state;
  }
}
