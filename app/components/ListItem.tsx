import React from 'react';
import { Text, View } from 'react-native';

interface Props {
  item: any
}

const ListItem = (props:Props) => {
  const { item } = props;
  return (
    <View style={[
      {
        width: '100%', flex: 1, paddingHorizontal: 8, paddingVertical: 5, marginVertical: 2, backgroundColor: '#F9E1E1',
      },
      { backgroundColor: item.is_running && 'green' },
    ]}
    >
      <Text>Project: {item.project.name}</Text>
      <Text>Hours: {item.hours}</Text>
      <Text>Active: {item.is_running ? 'Yes' : 'No'}</Text>
    </View>
  );
};

export default ListItem;
