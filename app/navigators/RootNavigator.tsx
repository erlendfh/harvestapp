import { createStackNavigator } from 'react-navigation';
import LoginScreen from '../screens/Login/LoginScreen';
import LoginWebView from '../screens/Login/LoginWebView';
import TabNavigator from './TabNavigator';

export default createStackNavigator({
  Login: {
    screen: LoginWebView,
  },
  Tab: TabNavigator,
}, {
  headerMode: 'none',
  initialRouteName: 'Login',
});
