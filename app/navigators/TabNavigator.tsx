import { createBottomTabNavigator } from 'react-navigation';
import LocationScreen from '../screens/Location/LocationScreen';
import HoursScreen from '../screens/Hours/HoursScreen';

export default createBottomTabNavigator({
  Location: {
    screen: LocationScreen,
  },
  Hours: {
    screen: HoursScreen,
  },
});
