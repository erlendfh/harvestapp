export const HARVEST_CONFIG = {
  headers: {
    'User-Agent': 'Node.js Harvest API Sample',
    'Harvest-Account-ID': '542650',
    'Content-type': 'application/json',
    Accept: 'application/json',
  },
};

export const HARVEST_ROOT_URL = 'https://api.harvestapp.com';
