import React, { PureComponent } from 'react';
import {
 View, Text, TextInput, Button, StyleSheet
} from 'react-native';
import { connect } from 'react-redux';
import { NavigationScreenProp } from 'react-navigation';
import { login } from '../../redux/authReducer';

interface Props {
  navigation: NavigationScreenProp<{}>,
  login: typeof login,
}

interface State {
  username: string,
  password: string,
}

class LoginScreen extends PureComponent<Props, State> {

  constructor(props:Props) {
    super(props);
    this.state = {
      username: '',
      password: '',
    };
  }

  onChangeUsername = (username:string) => this.setState({ username })
  onChangePassword = (password:string) => this.setState({ password })
  onLoginPressed = () => this.props.login(this.state.username, this.state.password)

  render() {
    return (
      <View style={styles.container}>
        <Text>Login</Text>
        <Text>Username:</Text>
        <TextInput
          style={styles.username}
          onChangeText={this.onChangeUsername}
          value={this.state.username}
          secureTextEntry={false}
        />
        <Text>Password:</Text>
        <TextInput
          style={styles.password}
          onChangeText={this.onChangePassword}
          value={this.state.password}
          secureTextEntry={true}
        />
        <Button
          title="Login"
          onPress={this.onLoginPressed}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  location: state.location,
  auth: state.auth,
});


export default connect(mapStateToProps, { login })(LoginScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: 'stretch',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2c3e50',
  },
  password: {
    textAlign: 'center',
    alignSelf: 'stretch',
    margin: 10,
    paddingHorizontal: 10,
    borderRadius: 10,
    lineHeight: 20,
    backgroundColor: 'white',
  },

  username: {
    textAlign: 'center',
    alignSelf: 'stretch',
    margin: 10,
    paddingHorizontal: 10,
    borderRadius: 10,
    height: 20,
    backgroundColor: 'white',
  }
});
