import React, { PureComponent } from 'react';
import { connect } from 'react-redux'
import {
  StyleSheet, WebView, NavState
} from 'react-native';
import { login } from '../../redux/authReducer';
import { NavigationScreenProp } from 'react-navigation';

const CLIENT_ID = '3YKHtxSZa2C-USB9QIWyZO-4'

interface Props {
  login: typeof login,
  navigation: NavigationScreenProp<{}>,
}

interface State {
}

class LoginWebView extends React.PureComponent<Props, State> {
  onNavigationStateChange = (navState:NavState) => {
    if (navState.url && navState.url.indexOf('https://www.auka.io/rndemoapp/loggedIn') === 0) {
      console.warn(navState.url)
      const [baseUrl, query] = navState.url.split('?')
      const args: {[index:string]: string} = {}
      query.split('&').forEach(part => {
        const [key, value] = part.split('=')
        args[key] = value
      })
      this.props.login(args['access_token'])
      this.props.navigation.navigate({routeName: 'Tab'})

    }
  }

  render() {
    return (<WebView
      onNavigationStateChange={this.onNavigationStateChange}
      style={styles.webview}
      source={{uri: `https://id.getharvest.com/oauth2/authorize?client_id=${CLIENT_ID}&response_type=token`}}
      />)
  }
}

export default connect(() => ({}), {
  login
})(LoginWebView)

const styles = StyleSheet.create({
  webview: {
    flex: 1,
    alignSelf: 'stretch',
  }
})