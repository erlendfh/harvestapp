import React, { PureComponent } from 'react';
import {
  View, Text, Button, FlatList, StyleSheet,
} from 'react-native';
import { connect } from 'react-redux';
import { NavigationScreenProp } from 'react-navigation';
import { updateLocationFailed, updateLocationSuccess } from '../../redux/locationReducer';
import ListItem from '../../components/ListItem';

interface Props {
  navigation: NavigationScreenProp<{}>,
  updateLocationSuccess: (position:Position) => void,
  updateLocationFailed: (error:PositionError) => void
}

interface State {
}
class LocationScreen extends PureComponent<Props, State> {
  watchId = 0

  constructor(props:Props) {
    super(props);
    this.state = {
    };
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchId);
  }

  fetchLocation = () => {
    this.watchId = navigator.geolocation.watchPosition(
      (position) => {
        this.props.updateLocationSuccess(position);
      },
      (error) => {
        this.props.updateLocationFailed(error);
      },
      {
        enableHighAccuracy: true,
        maximumAge: 1000,
        timeout: 20000,
      },
    );
  }

  render() {
    const { navigation, location } = this.props;
    if (!location.position) {
      return (
        <View style={styles.container}>
          <Button
            title="Add current location"
            onPress={this.fetchLocation}
          />
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <Text> {location.position.coords.latitude} </Text>
        <Text> {location.position.coords.longitude} </Text>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  location: state.location,
});


export default connect(mapStateToProps,
  { updateLocationSuccess, updateLocationFailed })(LocationScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2c3e50',
  },
});
