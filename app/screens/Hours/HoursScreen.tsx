import React, { Component } from 'react';
import {
  ScrollView, Text, Button, FlatList, View, ActivityIndicator, StyleSheet,
} from 'react-native';
import { connect } from 'react-redux';
import colors from '../../utils/colors';
import { fetchHours } from '../../redux/harvestReducer';
import ListItem from '../../components/ListItem';

class HoursScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount() {
    this.props.fetchHours();
  }


  render() {
    const { harvest } = this.props;
    if (!harvest.hours) {
      return (
        <View style={styles.container}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      );
    }
    return (
      <FlatList
        style={styles.scrollViewContainer}
        data={harvest.hours.time_entries}
        renderItem={({ item }) => (
          <ListItem
            key={item.id}
            item={item}
          />
        )}
      />
    );
  }
}

const mapStateToProps = state => ({
  location: state.location,
  harvest: state.harvest,
});


export default connect(mapStateToProps, { fetchHours })(HoursScreen);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.darkBlue,
  },

  scrollViewContainer: {
    flex: 1,
    backgroundColor: colors.darkBlue,
    paddingTop: 20,
  },
});
