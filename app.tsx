import React, { Component } from 'react';
import {
  Platform, StyleSheet, Text, View,
} from 'react-native';
import { Provider } from 'react-redux';
import logger from 'redux-logger';
import { createStore, applyMiddleware } from 'redux';
import axios from 'axios';
import createSagaMiddleware from 'redux-saga';
import RootNavigator from './app/navigators/RootNavigator';
import rootSaga from './app/sagas';
import reducers from './app/redux/rootReducer';

const sagaMiddleware = createSagaMiddleware();

const createStoreWithMiddleware = createStore(
  reducers,
  applyMiddleware(sagaMiddleware, logger),
);

axios.interceptors.request.use(request => {
  console.log('Starting Request', request)
  return request
})

axios.interceptors.response.use(response => {
  console.log('Response:', response)
  return response
})

interface Props {}
export default class App extends Component<Props> {
  render() {
    return (
      <Provider store={createStoreWithMiddleware}>
        <RootNavigator />
      </Provider>

    );
  }
}

sagaMiddleware.run(rootSaga);
